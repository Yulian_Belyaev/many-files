import java.io.*;

/**
 * Created by g15oit18 on 08.06.2017.
 */
public class manyFiles2 {
    public static void main(String[] args) throws IOException {
        File intdata = new File("src\\intdata.dat");
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(intdata));
        DataInputStream dataReader = new DataInputStream(new FileInputStream("src\\intdata.dat"));
        int number;

        while (dataReader.available() > 0) {
            number = dataReader.readInt();
            if (number == 0) {
                dataOutputStream.writeInt(number);
            } else if (number > 999 && number < 1000000) {
                dataOutputStream.writeInt(number);
            }
        }
        dataReader.close();
        dataOutputStream.close();
    }
}
